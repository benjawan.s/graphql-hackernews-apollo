import React from 'react';
import ReactDOM from 'react-dom';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import {
  ApolloProvider,
  ApolloClient,
  createHttpLink,
  InMemoryCache,
  split,
} from '@apollo/client';
import { WebSocketLink } from '@apollo/link-ws';
import { setContext } from '@apollo/client/link/context';
import './styles/index.css';
import { getMainDefinition } from '@apollo/client/utilities';
import App from './components/App';
import reducer from './reducers';

const store = createStore(reducer);

const httpLink = createHttpLink({
  uri: 'http://localhost:8080/query',
});

const wsLink = process.browser
  ? new WebSocketLink({
      uri: 'ws://localhost:8080/query',
      options: {
        reconnect: true,
      },
    })
  : null;

const authLink = setContext((_, { headers }) => {
  // eslint-disable-next-line no-undef
  const token = localStorage.getItem('token');
  return {
    headers: {
      ...headers,
      Authorization: token || '',
    },
  };
});

const splitLink = process.browser
  ? split(
      ({ query }) => {
        const definition = getMainDefinition(query);
        return (
          definition.kind === 'OperationDefinition' &&
          definition.operation === 'subscription'
        );
      },
      wsLink,
      authLink.concat(httpLink),
    )
  : authLink.concat(httpLink);

const client = new ApolloClient({
  link: splitLink,
  cache: new InMemoryCache(),
});

ReactDOM.render(
  <React.StrictMode>
    <ApolloProvider client={client}>
      <Provider store={store}>
        <App />
      </Provider>
    </ApolloProvider>
  </React.StrictMode>,
  // eslint-disable-next-line no-undef
  document.getElementById('root'),
);
