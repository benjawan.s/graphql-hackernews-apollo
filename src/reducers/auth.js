import { LOGIN, LOGOUT } from '../constants/actionTypes';

const initialState = {
  token: '',
};

export default (state = initialState, action) => {
  switch (action.type) {
    case LOGIN:
      // eslint-disable-next-line no-undef
      localStorage.setItem('token', action.token);
      return {
        ...state,
        token: action.token,
      };
    case LOGOUT:
      // eslint-disable-next-line no-undef
      localStorage.removeItem('token');
      return {
        ...state,
        token: '',
      };
    default:
      return state;
  }
};
