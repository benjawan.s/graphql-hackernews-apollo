import * as types from '../constants/actionTypes';

export const login = (token) => ({ type: types.LOGIN, token });
export const logout = () => ({ type: types.LOGOUT });
