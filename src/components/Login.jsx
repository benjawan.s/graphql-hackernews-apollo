import React, { useState } from 'react';
import { useMutation, gql } from '@apollo/client';
import { connect } from 'react-redux';
import { login } from '../actions';

const LOGIN_MUTATION = gql`
  mutation LoginMutation($username: String!, $password: String!) {
    login(input: { username: $username, password: $password })
  }
`;

const Login = ({ login: loginAction }) => {
  const [statusMessage, setStatusMessage] = useState('');
  const [formState, setFormState] = useState({
    username: '',
    password: '',
  });
  const [loginMutation] = useMutation(LOGIN_MUTATION, {
    variables: {
      username: formState.username,
      password: formState.password,
    },
    onCompleted: (data) => {
      loginAction(data.login);
    },
    onError: (err) => {
      setStatusMessage(err.message);
    },
  });

  return (
    <div>
      <form
        onSubmit={(e) => {
          e.preventDefault();
          loginMutation();
        }}
      >
        <div className="flex flex-column mt3">
          <h2>Sign in</h2>
        </div>
        <div className="flex flex-column mt3">
          <input
            className="mb2"
            value={formState.username}
            onChange={(e) => {
              setFormState({
                ...formState,
                username: e.target.value,
              });
            }}
            type="text"
            placeholder="Username"
          />
          <input
            className="mb2"
            value={formState.password}
            onChange={(e) => {
              setFormState({
                ...formState,
                password: e.target.value,
              });
            }}
            type="password"
            placeholder="Password"
          />
        </div>
        <button type="submit">Login</button>
      </form>
      {statusMessage && <span>{statusMessage}</span>}
    </div>
  );
};

const mapStateToProps = () => ({});
const mapDispatchToProps = (dispatch) => ({
  login: (token) => dispatch(login(token)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);
