import React from 'react';
import { DateTime } from 'luxon';

const Link = (props) => {
  const { link } = props;
  return (
    <div className="pv2">
      <div>{link.title}</div>
      <div>
        (<a href={link.url}>{link.url}</a>)
      </div>
      <div>Published by {link.user.name}</div>
      <div>
        Published at{' '}
        {DateTime.fromISO(link.createdAt).toLocaleString(
          DateTime.DATETIME_FULL,
        )}
      </div>
    </div>
  );
};

export default Link;
