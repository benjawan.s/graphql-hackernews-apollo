import React, { useState } from 'react';
import { useMutation, gql } from '@apollo/client';

const CREATE_LINK_MUTATION = gql`
  mutation CreateLinkMutation($title: String!, $url: String!) {
    createLink(input: { title: $title, url: $url }) {
      id
      title
      url
      user {
        name
      }
    }
  }
`;

const CreateLink = () => {
  const [statusMessage, setStatusMessage] = useState('');
  const [formState, setFormState] = useState({
    title: '',
    url: '',
  });
  const [createLink] = useMutation(CREATE_LINK_MUTATION, {
    variables: {
      title: formState.title,
      url: formState.url,
    },
    onCompleted: () => {
      setStatusMessage('News created successfully!');
    },
    onError: (apolloErr) => {
      setStatusMessage(apolloErr.message);
    },
  });

  return (
    <div>
      <form
        onSubmit={(e) => {
          e.preventDefault();
          createLink();
          setFormState({ title: '', url: '' });
          e.target.reset();
        }}
      >
        <div className="flex flex-column mt3">
          <h2>Create news</h2>
        </div>
        <div className="flex flex-column mt3">
          <input
            className="mb2"
            value={formState.title}
            onChange={(e) => {
              setFormState({ ...formState, title: e.target.value });
            }}
            type="text"
            placeholder="A news title"
          />
          <input
            className="mb2"
            value={formState.url}
            onChange={(e) => {
              setFormState({ ...formState, url: e.target.value });
            }}
            type="text"
            placeholder="A news url"
          />
        </div>
        <button type="submit">Submit</button>
        <div>{statusMessage && <span>{statusMessage}</span>}</div>
      </form>
    </div>
  );
};

export default CreateLink;
