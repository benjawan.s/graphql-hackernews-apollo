import React, { useState } from 'react';
import { useMutation, gql } from '@apollo/client';

const CREATE_USER_MUTATION = gql`
  mutation CreateUserMutation($username: String!, $password: String!) {
    createUser(input: { username: $username, password: $password })
  }
`;

const CreateUser = () => {
  const [statusMessage, setStatusMessage] = useState('');
  const [formState, setFormState] = useState({
    username: '',
    password: '',
  });
  const [createUser] = useMutation(CREATE_USER_MUTATION, {
    variables: {
      username: formState.username,
      password: formState.password,
    },
    onCompleted: () => {
      setStatusMessage('User created successfully!');
    },
    onError: (apolloErr) => {
      setStatusMessage(apolloErr.message);
    },
  });

  return (
    <div>
      <form
        onSubmit={(e) => {
          e.preventDefault();
          createUser();
          setFormState({ username: '', password: '' });
          e.target.reset();
        }}
      >
        <div className="flex flex-column mt3">
          <h2>Sign up</h2>
        </div>
        <div className="flex flex-column mt3">
          <input
            className="mb2"
            value={formState.username}
            onChange={(e) => {
              setFormState({
                ...formState,
                username: e.target.value,
              });
            }}
            type="text"
            placeholder="Username"
          />
          <input
            className="mb2"
            value={formState.password}
            onChange={(e) => {
              setFormState({ ...formState, password: e.target.value });
            }}
            type="password"
            placeholder="Password"
          />
        </div>
        <button type="submit">Submit</button>
        <div>{statusMessage && <span>{statusMessage}</span>}</div>
      </form>
    </div>
  );
};

export default CreateUser;
