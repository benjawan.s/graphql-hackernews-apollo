import React from 'react';
import { connect } from 'react-redux';
import CreateLink from './CreateLink';
import CreateUser from './CreateUser';
import Login from './Login';
import LinkList from './LinkList';
import { logout } from '../actions';

function App({ isLoggedIn, logout: logoutAction }) {
  if (isLoggedIn) {
    return (
      <div className="flex items-center flex-column">
        <div className="pa4">
          <button type="button" onClick={() => logoutAction()}>
            Logout
          </button>
        </div>
        <div>
          <CreateLink />
          <LinkList />
        </div>
      </div>
    );
  }

  return (
    <div className="flex items-center flex-column">
      <Login />
      <CreateUser />
    </div>
  );
}

const mapStateToProps = ({ auth }) => ({
  isLoggedIn: !!auth.token,
});
const mapDispatchToProps = (dispatch) => ({
  logout: () => dispatch(logout()),
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
