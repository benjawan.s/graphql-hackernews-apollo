import React, { useEffect } from 'react';
import { useQuery, gql } from '@apollo/client';
import { v4 as uuidv4 } from 'uuid';
import Link from './Link';

const LINKS_QUERY = gql`
  {
    links {
      id
      title
      url
      createdAt
      user {
        name
      }
    }
  }
`;

const LINKS_SUBSCRIPTION = gql`
  subscription($id: ID!) {
    linkAdded(id: $id) {
      id
      title
      url
      createdAt
      user {
        name
      }
    }
  }
`;

const LinkList = () => {
  const { data, loading, subscribeToMore } = useQuery(LINKS_QUERY);

  useEffect(() => {
    let unsubscribe;
    if (subscribeToMore) {
      unsubscribe = subscribeToMore({
        document: LINKS_SUBSCRIPTION,
        variables: {
          id: uuidv4(),
        },
        updateQuery: (prev, { subscriptionData }) => {
          if (!subscriptionData) return prev;
          const newLinkItem = subscriptionData.data.linkAdded;
          return { ...prev, links: [newLinkItem, ...prev.links] };
        },
      });
    }

    return function cleanup() {
      if (unsubscribe) {
        unsubscribe();
      }
    };
  }, [subscribeToMore]);

  if (loading) return <div>Loading...</div>;

  return (
    <div>
      <div className="flex flex-column mt3">
        <h2>News</h2>
      </div>
      {data && (
        <>
          {data.links.map((link) => (
            <Link key={link.id} link={link} />
          ))}
        </>
      )}
    </div>
  );
};

export default LinkList;
